from flask import Flask, render_template, request
import requests
from flask_cors import CORS
import config

app = Flask(__name__, template_folder='templates', static_folder='static' )

@app.route('/listarCentral', methods=['GET'])
def listarCentral():
    centrales = requests.get('https://central-api-yjqde4672q-uc.a.run.app/central').json()
    return render_template('index.html', centrales=centrales)


@app.route('/crearCentrales', methods=['GET'])
def crearCentrales():
    return render_template('crearCentral.html')


@app.route('/guardarCentrales', methods=['POST'])
def guardarCentrales():

    central = dict(request.values)
    
    central['origen'] = str(request.form['origen'])
    central['destinatario'] = str(request.form['destinatario'])
    central['estado_conexion'] = str(request.form['estado_conexion'])
    central['estado_actual'] = str(request.form['estado_actual'])
    central['fecha'] = str(request.form['fecha'])

    requests.post('https://central-api-yjqde4672q-uc.a.run.app/central', json=central)

    return (listarCentral())


app.run(host="0.0.0.0", port=config.PORT, debug=config.DEBUG_MODE)